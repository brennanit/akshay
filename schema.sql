CREATE TABLE customer (
				id INT( 11 ) AUTO_INCREMENT PRIMARY KEY ,
				name VARCHAR( 255 ) NOT NULL ,
				email VARCHAR( 255 ) NOT NULL ,
				dob DATE NOT NULL ,
				last_updated DATETIME NOT NULL);
